package gambit.modbus.listRegister;

import android.view.View;
import android.widget.TextView;

import gambit.modbus.R;
import gambit.modbus.model.RegisterItem;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

public class RegisterViewBinder extends ViewBinder<RegisterItem, RegisterViewBinder.ViewHolder> {

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter adapter, ViewHolder holder, int position, RegisterItem entity) {
        holder.txtName.setText(entity.getName());
        holder.txtValue.setText(entity.getValue());
        holder.txtNote.setText(entity.getNote());
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.adapter_register_item;
    }


    static class ViewHolder extends ViewBinder.ViewHolder {
        TextView txtName;
        TextView txtValue;
        TextView txtNote;

        public ViewHolder(View rootView) {
            super(rootView);
            this.txtName = rootView.findViewById(R.id.txtName);
            this.txtValue = rootView.findViewById(R.id.txtValue);
            this.txtNote = rootView.findViewById(R.id.txtNote);
        }

    }
}
