package gambit.modbus.listRegister;

import android.view.View;
import android.widget.TextView;

import gambit.modbus.R;
import gambit.modbus.model.RegisterHeader;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

public class RegisterHeaderViewBinder extends ViewBinder<RegisterHeader, RegisterHeaderViewBinder.ViewHolder> {

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter adapter, ViewHolder holder, int position, RegisterHeader entity) {
        holder.txtDate.setText(entity.getDate());
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.adapter_register_header;
    }


    static class ViewHolder extends ViewBinder.ViewHolder {
        TextView txtDate;

        public ViewHolder(View rootView) {
            super(rootView);
            this.txtDate = rootView.findViewById(R.id.txtDate);
        }

    }
}
