package gambit.modbus.ui.activities.controller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import gambit.modbus.R;
import gambit.modbus.ui.fragments.listRegister.ListRegisterFragment;

public class ControllerActivity extends AppCompatActivity {

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);
        unbinder = ButterKnife.bind(this);

        if (savedInstanceState == null) {
            Fragment fragment = ListRegisterFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.mainContent, fragment)
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }
}
