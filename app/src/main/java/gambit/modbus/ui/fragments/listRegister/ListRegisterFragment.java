package gambit.modbus.ui.fragments.listRegister;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import gambit.modbus.R;
import gambit.modbus.listRegister.RegisterHeaderViewBinder;
import gambit.modbus.listRegister.RegisterViewBinder;
import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

public class ListRegisterFragment extends MvpAppCompatFragment implements ListRegisterView {
    @InjectPresenter
    ListRegisterPresenter presenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progress)
    ProgressBar progress;

    private Unbinder unbinder;
    private StickyHeaderViewAdapter adapter;

    public static ListRegisterFragment newInstance() {
        return new ListRegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_register, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void setupAdapter(List<DataBean> list) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new StickyHeaderViewAdapter(list)
                .RegisterItemType(new RegisterViewBinder())
                .RegisterItemType(new RegisterHeaderViewBinder());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showProgress(boolean isShow) {
        if (isShow)
            progress.setVisibility(View.VISIBLE);
        else
            progress.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

}
