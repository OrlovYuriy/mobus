package gambit.modbus.ui.fragments.listRegister;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.common.primitives.Ints;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import gambit.modbus.helper.ErrorRegister;
import gambit.modbus.helper.LanguageRegister;
import gambit.modbus.helper.TypeRegister;
import gambit.modbus.model.DataItem;
import gambit.modbus.model.Model;
import gambit.modbus.model.Register;
import gambit.modbus.model.RegisterHeader;
import gambit.modbus.model.RegisterItem;
import gambit.modbus.model.Test;
import tellh.com.stickyheaderview_rv.adapter.DataBean;

@InjectViewState
public class ListRegisterPresenter extends MvpPresenter<ListRegisterView> {
    static {
        System.loadLibrary("native-lib");
    }
    public native String parsingREAL4(int[] arr);
    public native String parsingLongUnsigned(int[] arr);
    public native String parsingLongSigned(int[] arr);
    public native long parsingCalendar(int[] arr);

    private SimpleDateFormat outputDate = new SimpleDateFormat("dd-MM-yyyy, HH:mm");

    @SuppressLint("StaticFieldLeak")
    public ListRegisterPresenter() {

        getViewState().showProgress(true);
        new AsyncTask<Void,Void,List<DataBean>>(){
            @Override
            protected List<DataBean> doInBackground(Void... voids) {
                return getListDataBean();
            }
            @Override
            protected void onPostExecute(List<DataBean> list) {
                super.onPostExecute(list);
                getViewState().showProgress(false);
                getViewState().setupAdapter(list);
            }
        }.execute();
    }

    private List<DataBean> getListDataBean() {
        List<DataBean> list = new ArrayList<>();
        List<Model> modelList = Test.getParsData();
        List<DataItem> dataList = Test.getData();

        for (int i = 0; i < dataList.size(); i++){
            DataItem dataItem = dataList.get(i);
            list.add(registerHeaderParsing(dataItem));
            for (int ii = 0; ii < modelList.size(); ii++) {
                Model model = modelList.get(ii);
                list.add(registerItemParsing(model, getListRegister(dataItem.getRegisters())));
            }
        }

        return list;
    }

    private RegisterHeader registerHeaderParsing(DataItem dataItem){
        return new RegisterHeader(dataItem.getData());
    }

    private RegisterItem registerItemParsing(Model model, List<Register> registerList) {
        List<Integer> values = new ArrayList<>();
        List<String> idRegisters = Arrays.asList(model.getRegisters().split(":"));
        String value="";

        for (int i = 0; i < registerList.size(); i++) {
            Register item = registerList.get(i);
            if (idRegisters.contains(item.getId())) values.add(item.getValue());
        }


        if(TypeRegister.LONG_PARSING_SIGNED==model.getTypePars()){
            value = parsingLongSigned(Ints.toArray(values));
        }
        else if(TypeRegister.LONG_PARSING_UNSIGNED==model.getTypePars()){
            value = parsingLongUnsigned(Ints.toArray(values));
        }
        else if(TypeRegister.INTEGER_PARSING_UNSIGNED==model.getTypePars()){
            value = parsingLongUnsigned(Ints.toArray(values));
        }
        else if (TypeRegister.REAL4_PARSING==model.getTypePars()){
            value = parsingREAL4(Ints.toArray(values));
        }
        else if (TypeRegister.INTEGER_PARSING_HIGH_BYTE==model.getTypePars()){
            value = ((values.get(0) >> 8) & 0xFF)+"";
        }
        else if (TypeRegister.INTEGER_PARSING_LOW_BYTE==model.getTypePars()){
            value = (values.get(0) & 0xFF)+"";
        }
        else if (TypeRegister.BCD_CALENDAR_PARSING==model.getTypePars()){
            long date = parsingCalendar(Ints.toArray(values));
            value = outputDate.format(new Date(date*1000));
        }
        else if (TypeRegister.ERROR_PARSING ==model.getTypePars()){
            value = ErrorRegister.getError(values.get(0));
        }
        else if (TypeRegister.LANGUAGE_PARSING ==model.getTypePars()){
            value = LanguageRegister.getLanguage(values.get(0));
        }
        return new RegisterItem(model.getName(), value, model.getNote());
    }

    private List<Register> getListRegister(List<String> listData) {
        List<Register> list = new ArrayList<>();

        if (listData == null) return list;
        for (int i = 0; i < listData.size(); i++) {
            String[] registerData = listData.get(i).split(":");
            if (registerData.length > 1) {
                try {
                    list.add(new Register(registerData[0], Integer.parseInt(registerData[1])));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }
}
