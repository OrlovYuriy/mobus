package gambit.modbus.ui.fragments.listRegister;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.DataBean;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface ListRegisterView extends MvpView {

    void setupAdapter(List<DataBean> list);
    void showProgress(boolean isShow);
}
