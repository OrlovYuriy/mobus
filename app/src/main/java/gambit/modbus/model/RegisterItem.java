package gambit.modbus.model;

import gambit.modbus.R;
import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

public class RegisterItem extends DataBean {
    private String name;
    private String value;
    private String note;

    public RegisterItem(String name, String value, String note) {
        this.name = name;
        this.value = value;
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getNote() {
        return note;
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.adapter_register_item;
    }

    @Override
    public boolean shouldSticky() {
        return false;
    }
}
