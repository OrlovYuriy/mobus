package gambit.modbus.model;

import gambit.modbus.R;
import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

public class RegisterHeader extends DataBean {
    private String date;

    public RegisterHeader(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.adapter_register_header;
    }

    @Override
    public boolean shouldSticky() {
        return true;
    }
}
