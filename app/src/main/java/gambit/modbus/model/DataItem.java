package gambit.modbus.model;

import java.util.List;

public class DataItem {
    private String data;
    private List<String> registers;

    public DataItem(String data, List<String> registers) {
        this.data = data;
        this.registers = registers;
    }

    public String getData() {
        return data;
    }

    public List<String> getRegisters() {
        return registers;
    }
}
