package gambit.modbus.model;

import java.util.ArrayList;
import java.util.List;

import gambit.modbus.helper.TypeRegister;

public class Test {

    public static List<DataItem> getData(){
        List<DataItem> list = new ArrayList<>();
        list.add(new DataItem("2017-01-11 19:12", getRegisters()));
        list.add(new DataItem("2016-11-18 16:23", getRegisters()));
        list.add(new DataItem("2016-11-10 17:43", getRegisters()));

        return list;
    }


    private static List<String> getRegisters(){
        List<String> list = new ArrayList<>();
        list.add("1:7579");
        list.add("2:48988");
        list.add("3:5064");
        list.add("4:48142");
        list.add("5:37967");
        list.add("6:48877");
        list.add("7:63814");
        list.add("8:17575");
        list.add("9:0");
        list.add("10:0");
        list.add("11:24224");
        list.add("12:15965");
        list.add("13:0");
        list.add("14:0");
        list.add("15:0");
        list.add("16:0");
        list.add("17:87");
        list.add("18:0");
        list.add("19:9891");
        list.add("20:16221");

        list.add("21:65480");
        list.add("22:65535");
        list.add("23:39041");
        list.add("24:48994");
        list.add("25:0");
        list.add("26:0");
        list.add("27:0");
        list.add("28:0");
        list.add("29:144");
        list.add("30:0");
        list.add("31:48777");
        list.add("32:16191");
        list.add("33:15568");
        list.add("34:16611");
        list.add("35:28424");
        list.add("36:16534");
        list.add("37:7424");
        list.add("38:15783");
        list.add("39:14592");
        list.add("40:15758");

        list.add("41:5461");
        list.add("42:49087");
        list.add("43:45184");
        list.add("44:15493");
        list.add("45:36608");
        list.add("46:15459");
        list.add("47:29184");
        list.add("48:15516");
        list.add("49:0");
        list.add("50:0");
        list.add("51:0");
        list.add("52:0");
        list.add("53:6432");
        list.add("54:4386");
        list.add("55:5889");
        list.add("56:0");
        list.add("57:0");
        list.add("58:0");
        list.add("59:0");
        list.add("60:255");

        list.add("61:120");
        list.add("62:0");
        list.add("63:0");
        list.add("64:0");
        list.add("65:0");
        list.add("66:4001");
        list.add("67:62500");
        list.add("68:0");
        list.add("69:0");
        list.add("70:3");
        list.add("71:4");
        list.add("72:4");
        list.add("73:3606");
        list.add("74:16800");
        list.add("75:54913");
        list.add("76:48896");
        list.add("77:35706");
        list.add("78:17101");
        list.add("79:44042");
        list.add("80:17099");

        list.add("81:33339");
        list.add("82:16963");
        list.add("83:42500");
        list.add("84:49530");
        list.add("85:33468");
        list.add("86:16963");
        list.add("87:33210");
        list.add("88:16963");
        list.add("89:2885");
        list.add("90:16512");
        list.add("91:0");
        list.add("92:806");
        list.add("93:3501");
        list.add("94:3501");
        list.add("95:0");
        list.add("96:1");
        list.add("97:43137");
        list.add("98:17105");
        list.add("99:3374");
        list.add("100:17839");

        return list;
    }

    public static List<Model> getParsData(){
        List<Model> list = new ArrayList<>();
        list.add(new Model("Flow Rate","REAL4","m3/h", "1:2",true, TypeRegister.REAL4_PARSING));
        list.add(new Model("Energy Flow Rate","REAL4","GJ/h", "3:4",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Velocity","REAL4","m/s", "5:6",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Fluid sound speed","REAL4","m/s", "7:8",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Positive accumulator","LONG","", "9:10",true,TypeRegister.LONG_PARSING_UNSIGNED));
        list.add(new Model("Positive decimal fraction","REAL4","", "11:12",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Negative accumulator","LONG","", "13:14",true,TypeRegister.LONG_PARSING_SIGNED));
        list.add(new Model("Negative decimal fraction","REAL4","", "15:16",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Positive energy accumulator","LONG","", "17:18",true,TypeRegister.LONG_PARSING_UNSIGNED));
        list.add(new Model("Positive energy decimal fraction","REAL4","", "19:20",true,TypeRegister.REAL4_PARSING));

        list.add(new Model("Negative energy accumulator","LONG","", "21:22",true,TypeRegister.LONG_PARSING_SIGNED));
        list.add(new Model("Negative energy decimal fraction","REAL4","", "23:24",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Net accumulator","LONG","", "25:26",true,TypeRegister.LONG_PARSING_SIGNED));
        list.add(new Model("Net decimal fraction","REAL4","", "27:28",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Net energy accumulator","LONG","", "29:30",true,TypeRegister.LONG_PARSING_SIGNED));
        list.add(new Model("Net energy decimal fraction","REAL4","", "31:32",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Temperature #1/inlet","REAL4","C", "33:34",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Temperature #2/outlet","REAL4","C", "35:36",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Analog input AI3","REAL4","", "37:38",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Analog input AI4","REAL4","", "39:40",true,TypeRegister.REAL4_PARSING));

        list.add(new Model("Analog input AI5","REAL4","", "41:42",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Current input at AI3","REAL4","mA", "43:44",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Current input at AI3","REAL4","mA", "45:46",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Current input at AI3","REAL4","mA", "47:48",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Calendar (date and time)","BCD","", "53:54:55",false,TypeRegister.BCD_CALENDAR_PARSING));

        list.add(new Model("Key to input","INTEGER","", "59",false,TypeRegister.INTEGER_PARSING_UNSIGNED));
        list.add(new Model("Go to Window #","INTEGER","", "60",false,TypeRegister.INTEGER_PARSING_UNSIGNED));

        list.add(new Model("LCD Back-lit lights for number of seconds","INTEGER","", "61",true,TypeRegister.INTEGER_PARSING_UNSIGNED));
        list.add(new Model("Times for the beeper","INTEGER","", "62",false,TypeRegister.INTEGER_PARSING_UNSIGNED));
        list.add(new Model("Pulses left for OCT","INTEGER","", "62",false,TypeRegister.INTEGER_PARSING_UNSIGNED));
        list.add(new Model("Error Code","BIT","", "72",true,TypeRegister.ERROR_PARSING));
        list.add(new Model("PT100 resistance of inlet","REAL4","Ohm", "77:78",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("PT100 resistance of outlet","REAL4","Ohm", "79:80",true,TypeRegister.REAL4_PARSING));

        list.add(new Model("Total travel time","REAL4","Micro-second", "81:82",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Delta travel time","REAL4","Micro-second", "83:84",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Upstream travel time","REAL4","Micro-second", "85:86",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Downstream travel time","REAL4","Micro-second", "87:88",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Output current","REAL4","mA", "89:90",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Working step","INTEGER","", "92",true,TypeRegister.INTEGER_PARSING_HIGH_BYTE));
        list.add(new Model("Signal Quality","INTEGER","", "92",true,TypeRegister.INTEGER_PARSING_LOW_BYTE));
        list.add(new Model("Upstream strength","INTEGER","", "93",true,TypeRegister.INTEGER_PARSING_UNSIGNED));
        list.add(new Model("Downstream strength","INTEGER","", "94",true,TypeRegister.INTEGER_PARSING_UNSIGNED));
        list.add(new Model("Language used in user interface","INTEGER","", "96",true,TypeRegister.LANGUAGE_PARSING));
        list.add(new Model("The rate of the measured travel time by the calculated travel time","REAL4","", "97:98",true,TypeRegister.REAL4_PARSING));
        list.add(new Model("Reynolds number","REAL4","", "99:100",true,TypeRegister.REAL4_PARSING));


        return list;
    }
}
