package gambit.modbus.model;

public class Model {
    private String name;
    private String format;
    private String note;
    private String registers;
    private boolean isShow;
    private int typePars;

    public Model(String name, String format, String note, String registers, boolean isShow, int typePars) {
        this.name = name;
        this.format = format;
        this.note = note;
        this.registers = registers;
        this.isShow = isShow;
        this.typePars = typePars;
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        return format;
    }

    public String getNote() {
        return note;
    }

    public String getRegisters() {
        return registers;
    }

    public boolean isShow() {
        return isShow;
    }

    public int getTypePars() {
        return typePars;
    }
}
