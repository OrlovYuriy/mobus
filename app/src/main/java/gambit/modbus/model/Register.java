package gambit.modbus.model;

public class Register {
    private String id;
    private int value;

    public Register(String id, int value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public int getValue() {
        return value;
    }
}
