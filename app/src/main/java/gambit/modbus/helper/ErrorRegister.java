package gambit.modbus.helper;

public class ErrorRegister {

    public static String getError(int code) {
        String error;
        switch (code){
            case 0:
                error = "no received signal";
                break;
            case 1:
                error = "low received signal";
                break;
            case 2:
                error = "poor received signal";
                break;
            case 3:
                error = "pipe empty";
                break;
            case 4:
                error = "hardware failure";
                break;
            case 5:
                error = "receiving circuits gain in adjusting";
                break;
            case 6:
                error = "frequency at the frequency output over flow";
                break;
            case 7:
                error = "current at 4-20mA over flow";
                break;
            case 8:
                error = "RAM check-sum error ";
                break;
            case 9:
                error = "main clock or timer clock error";
                break;
            case 10:
                error = "parameters check-sum error";
                break;
            case 11:
                error = "ROM check-sum error";
                break;
            case 12:
                error = "temperature circuits error";
                break;
            case 13:
                error = "reserved";
                break;
            case 14:
                error = "internal timer over flow";
                break;
            default:
                error = "";
        }

        return error;
    }
}
