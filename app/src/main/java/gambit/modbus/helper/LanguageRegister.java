package gambit.modbus.helper;

public class LanguageRegister {
    public static String getLanguage(int code) {
        String language;
        switch (code) {
            case 0:
                language = "English";
                break;
            case 1:
                language = "Chinese";
                break;
            default:
                language = "English";
        }

        return language;
    }
}
