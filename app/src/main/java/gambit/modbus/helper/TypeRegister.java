package gambit.modbus.helper;

public class TypeRegister {
    public final static int NOT_PARSING = 0;
    public final static int LONG_PARSING_SIGNED = 1;
    public final static int LONG_PARSING_UNSIGNED = 2;
    public final static int REAL4_PARSING = 3;
    public final static int INTEGER_PARSING_SIGNED = 4;
    public final static int INTEGER_PARSING_UNSIGNED = 5;
    public final static int INTEGER_PARSING_HIGH_BYTE = 6;
    public final static int INTEGER_PARSING_LOW_BYTE = 7;
    public final static int BCD_PARSING = 8;
    public final static int ERROR_PARSING = 9;
    public final static int LANGUAGE_PARSING = 10;
    public final static int BCD_CALENDAR_PARSING = 11;
}
