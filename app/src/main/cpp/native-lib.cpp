#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_gambit_modbus_ui_fragments_listRegister_ListRegisterPresenter_parsingREAL4(JNIEnv *env, jobject, jintArray arr) {
    unsigned int data;
    float result;
    jsize len = env->GetArrayLength(arr);
    jint *body = env->GetIntArrayElements(arr, 0);

    data = body[len-1];
    for (int i=len-2; i>=0; i--) {
        data =(data << 16) | body[i];
    }
    memcpy(&result, &data, sizeof(float));

    return env->NewStringUTF(std::to_string(result).c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_gambit_modbus_ui_fragments_listRegister_ListRegisterPresenter_parsingLongUnsigned(JNIEnv *env, jobject, jintArray arr) {
    unsigned int result;
    jsize len = env->GetArrayLength(arr);
    jint *body = env->GetIntArrayElements(arr, 0);

    result = body[len-1];
    for (int i=len-2; i>=0; i--) {
        result =(result << 16) | body[i];
    }

    return env->NewStringUTF(std::to_string(result).c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_gambit_modbus_ui_fragments_listRegister_ListRegisterPresenter_parsingLongSigned(JNIEnv *env, jobject, jintArray arr) {
    int result;
    jsize len = env->GetArrayLength(arr);
    jint *body = env->GetIntArrayElements(arr, 0);

    result = body[len-1];
    for (int i=len-2; i>=0; i--) {
        result =(result << 16) | body[i];
    }
    return env->NewStringUTF(std::to_string(result).c_str());
}

extern "C" JNIEXPORT jlong JNICALL
Java_gambit_modbus_ui_fragments_listRegister_ListRegisterPresenter_parsingCalendar(JNIEnv *env, jobject, jintArray arr) {
    unsigned long result;
    jsize len = env->GetArrayLength(arr);
    jint *body = env->GetIntArrayElements(arr, 0);

    result = body[len-1];
    for (int i=len-2; i>=0; i--) {
        result =(result << 8) | (body[i] & 0xFF);
        result =(result << 8) | ((body[i] >> 8) & 0xFF);
    }
    return result;
}